import time
import numpy as np

matriz1 = np.random.rand(1000, 1000)
matriz2 = np.random.rand(1000, 1000)

c_tf = np.matmul(matriz1, matriz2)

#CPU
start_time = time.time()
c = np.matmul(matriz1, matriz2)
cpu_time = time.time() - start_time

# Measure the GPU time
start_time = time.time()
c_tf = np.matmul(matriz1, matriz2)
gpu_time = time.time() - start_time

print(f'CPU time: {cpu_time} seconds')
print(f'GPU time: {gpu_time} seconds')