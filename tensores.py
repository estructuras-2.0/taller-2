import time
import numpy as np
import tensorflow as tf
import os

tensor1 = tf.random.normal([1000, 1000])
tensor2 = tf.random.normal([1000, 1000])

# CPU
start_time_cpu = time.time()
resultado = tf.matmul(tensor1, tensor2)
cpu_time = time.time() - start_time_cpu

# TPU
start_time_tpu = time.time()
resolver_tpu = tf.distribute.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR'])
tf.config.experimental_connect_to_cluster(resolver_tpu)
tf.tpu.experimental.initialize_tpu_system(resolver_tpu)
strategy = tf.distribute.TPUStrategy(resolver_tpu)
with strategy.scope():
   resultado = tf.matmul(tensor1, tensor2)
tpu_time = time.time() - start_time_tpu

# Imprimir resultados
print("CPU Resultado:", resultado)
print(f'Tiempo de ejecución en CPU: {cpu_time} segundos')
print(f'Tiempo de ejecución en TPU: {tpu_time} segundos')
